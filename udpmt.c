/***************************************************************************
 *
 *  $Id$
 *
 *  Created byGilles Berger Sabbatel
 *
 *  This file is part of the ipmt package
 *
 * Copyright LIG Laboratory (2007)
 *
 * This software is a computer program whose purpose is to provide a set
 * of tools to measure performance of networks at the transport level
 * (TCP/IP and UDP/IP)..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 ***************************************************************************/

#include <signal.h>
#include <stdio.h>
#include <sys/select.h>
#include <time.h>

#include "ipmt.h"
#include "testfile.h"
#include "ticker.h"

static int p_burstsz = 0;  // bursts size (number of packets)
static int nobufs = 0;
static int ius = 0;     // interval between transmissions
static int gpause = 0;  // interval between batches

#ifdef HAVE_LINUX_WIRELESS_H
static int wireless = 0;  // print wireless parameters
#endif

static FILE *test_file = 0;

static long long rate = 0;  // bitrate

static struct addrinfo hints, *rai = NULL;

static void send_magic_packet() {
    memset(buf, 0xFF, MAGIC_SIZE);
    while (sendto(sock, buf, MAGIC_SIZE, 0, rai->ai_addr, rai->ai_addrlen) < 0) {
        if (errno == ENOBUFS) {
            nobufs++;
            continue;
        }
        perror("sendto");
    }
    fprintf(stderr, "Nobufs = %d\n", nobufs);
}

// Write end of the termination pipe;
static volatile int pipe_w = -1;

// Workaround used because there's still exists from external functions. It
// helps to know the functions want to exit before entering a loop.
static volatile sig_atomic_t quit = 0;

static void quit_handler() {
    quit = 1;
    if (pipe_w >= 0) {
        close(pipe_w);
    }
}

// FIXME: finish() is still called from multiple functions, eventually get rid of it.
void finish() { quit_handler(); }

/*
 * Display usage message
 */

static void usage() {
    printf("Usage: %s [options] host\n"
               "Options :\n"
               "    -4       : force IPv4 protocol\n"
               "    -6       : force IPv6 protocol\n"
               "    -B burst : burst size, in packets (default " STR(DEFAULT_PKT_BURST_SZ) ")\n"
               "    -b burst : burst size, in bytes (default " STR(DEFAULT_PKT_BURST_SZ) " packet)\n"
               "    -c       : allow sending broadCasts\n"
               "    -D dscp  : diff serv code point (RFC 2474)\n"
               "    -d secs  : test duration in seconds (default transmit until stopped)\n"
               "    -f file  : reads traffic description from a file\n"
               "    -i us    : interval between transmissions, in microseconds (default: 0)\n"
               "    -n pknb  : total number of packets to send (default: transmit until stopped)\n"
               "    -P msecs : instead of stopping after sending -n pknb, wait for this duration\n"
               "    -p port  : destination port number (default: " DEFAULT_PORT ")\n"
               "    -r rate  : rate in Kbit/s (K = " STR(K) ")\n"
               "    -S source: [host][:port] format\n"
               "    -s pktsz : packet size in bytes (default v4: " STR(DEFAULT_PKT_SZ_4) ", v6: " STR(DEFAULT_PKT_SZ_6) ")\n"
               "    -T tos   : type of service (deprecated: RFC 1349, obsoleted by RFC 2474)\n"
               "    -t int   : timer interval in microseconds (default " STR(MIN_INTERVAL) ")\n"
               "    -V       : be Verbose\n"
               "    -v       : display version number and exits\n"
#ifdef HAVE_LINUX_WIRELESS_H
               "    -w int   : display wireless conditions every int ms\n"
#endif
               , execname);
    exit(1);
}

static void maketimestamp() {
    struct timeval tv;

    gettimeofday(&tv, NULL);
    pkt_hdr->ts_pkt_sec = htonl(tv.tv_sec);
    pkt_hdr->ts_pkt_usec = htonl(tv.tv_usec);
}

/*
 *  Send one burst
 */

// send_one_burst sends one burst of packets and returns true if it is the last one.
static int send_one_burst() {
    // The scope of these variables is restricted to the function but the
    // content is preserved between calls.
    static int burst_remain = 0;  // remains after burst
    static int prev_nb_pkt = 1;

    int i;

    if (nb_pkt > 0 && nb_sent >= nb_pkt + prev_nb_pkt) {
        if (gpause > 0) {
            add_time(&next_event, &next_event, gpause * 1000);
            prev_nb_pkt = nb_sent;
        } else {
            return 1;
        }
    }
    for (i = burst_remain; i < b_burstsz; i += pkt_sz) {
        pkt_hdr->flags = 0;
        pkt_hdr->seqnum = htonl(nb_sent);

        if (i == burst_remain) {  // 1st packet
            pkt_hdr->flags |= FIRST_IN_BURST;
        }
        if (i + pkt_sz >= b_burstsz) {  // last packet
            pkt_hdr->flags |= LAST_IN_BURST;
        }
        if (nb_sent == prev_nb_pkt) {
            pkt_hdr->flags |= LAST_IN_BATCH;
        }
        if (nb_sent == prev_nb_pkt + 1) {
            pkt_hdr->flags |= FIRST_IN_BATCH;
        }

        maketimestamp();
        while (sendto(sock, buf, pkt_sz, 0, rai->ai_addr, rai->ai_addrlen) < 0) {
            if (errno == ENOBUFS) {
                nobufs++;
                maketimestamp();
                continue;
            }
            perror("Sendto");
            terminate();
        }
        nb_sent++;
        smooth_win[cycle].nb_pkt++;
    }
    burst_remain = i - b_burstsz;
    gettimeofday(&current_t, NULL);

#ifdef HAVE_LINUX_WIRELESS_H
    if (wireless)
        wireless_stats();
#endif
    display_stats();
    return 0;
}

/*
 * Compute time to the next burst, either for regular transmission, or
 * if reading a test file
 */
static int time_to_next_burst() {
    if (!test_file) {
        return ius;
    } else {
        return 1000 * prepare_next_burst(1);
    }
}

// ticker_handlers is called by the ticker when an ALRM event is fired.
static void ticker_handler() {
    int end = 0;
    int delta = 0;
    struct timeval time;

    gettimeofday(&time, NULL);
    // Send all bursts that are due for delivery.
    while (posterieur(time, next_event)) {
        end = send_one_burst();
        delta = time_to_next_burst();
        if (end || delta < 0) {
            // We're already in a signal handler, we can call it safely.
            quit_handler();
            return;
        }
        add_time(&next_event, &next_event, delta);
    }
}

// main is the program entry point.
int main(int argc, char **argv) {
    char defaultportname[] =
        DEFAULT_PORT;  // port number -- has to be long enough to fit most services in /etc/services :-)
    portname = defaultportname;
    source = NULL;  // force source [host][:port]
    char *testfilename;
    testfilename = NULL;
    int tos = 0;  // type of service

    int i, default_pkt_sz, ch, bcast = 0;

    long long ll, krate;

    int timer_interval = MIN_INTERVAL;
    duration = 0;
    pkt_sz = 0;
    nb_pkt = -1;
    force_ipv4 = 0;
    force_ipv6 = 0;
    sender_disp = 0;

    strncpy(execname, basename(argv[0]), sizeof(execname) - 1);
    execname[sizeof(execname) - 1] = '\0';
    while ((ch = getopt(argc, argv, "p:s:n:i:B:b:d:t:r:w:T:D:v46hcVP:S:f:")) != -1) {
        switch (ch) {
            case 'B':  // bursts size in packets
                if (opt_to_int(optarg, &p_burstsz) < 0) {
                    fprintf(stderr, "burst size should be a numeric value\n");
                    exit(1);
                }
                if (p_burstsz <= 0) {
                    fprintf(stderr, "Size of bursts should be > 0\n");
                    exit(2);
                }
                break;
            case 'b':  // bursts size in bytes
                if (opt_to_uint(optarg, &b_burstsz) < 0) {
                    fprintf(stderr, "burst size should be a numeric value\n");
                    exit(1);
                }
                if (b_burstsz < 4) {
                    fprintf(stderr, "Size of bursts should be >= 4\n");
                    exit(2);
                }
                break;
            case 'd':  //  test duration
                if (opt_to_int(optarg, &duration) < 0) {
                    fprintf(stderr, "duration should be a numeric value\n");
                    exit(1);
                }
                if (duration < 0) {
                    fprintf(stderr, "Duration should be >= 0\n");
                    exit(2);
                }
                break;
            case 'P':  //  gpauses duration
                if (opt_to_int(optarg, &gpause) < 0) {
                    fprintf(stderr, "Pause duration should be a numeric value\n");
                    exit(1);
                }
                if (duration < 0) {
                    fprintf(stderr, "Pause duration should be >= 0\n");
                    exit(2);
                }
                break;
            case 'i':  // interval
                if (opt_to_int(optarg, &ius) < 0) {
                    fprintf(stderr, "interval should be a numeric value\n");
                    exit(1);
                }
                if (ius < 0) {
                    fprintf(stderr, "Interval should be >= 0\n");
                    exit(2);
                }
                break;
            case 'n':  // number of blocks
                if (opt_to_int(optarg, &nb_pkt) < 0) {
                    fprintf(stderr, "Number of blocks should be a numeric value\n");
                    exit(1);
                }
                if (nb_pkt == 0) {
                    printf("Number of blocks to send should be > 0\n");
                    exit(2);
                }
                break;
            case 'p':  // port number
                portname = optarg;
                break;
            case 'r':  // bitrate
                if (opt_to_longlong(optarg, &rate) < 0) {
                    fprintf(stderr, "rate should be a numeric value\n");
                    exit(1);
                }
                if (rate < 0) {
                    fprintf(stderr, "Rate should be > 0\n");
                    exit(2);
                }
                break;
            case 's':  // blocks size
                if (opt_to_uint(optarg, &pkt_sz) < 0) {
                    fprintf(stderr, "block size should be a numeric value\n");
                    exit(1);
                }
                if (pkt_sz <= 0) {
                    fprintf(stderr, "Block size should be >= 0\n");
                    exit(2);
                }
                if (pkt_sz < 4) {
                    fprintf(stderr,
                            "Pkt size < 4 : cannot carry "
                            "sequence numbers...\n");
                    fprintf(stderr, "Target will not display lost packets.\n");
                }

                if (pkt_sz > MAX_OCTETS) {
                    fprintf(stderr, "Block size limit is %d\n", MAX_OCTETS);
                    exit(2);
                }
                break;
            case 't':  // timer interval
                if (opt_to_int(optarg, &timer_interval) < 0) {
                    fprintf(stderr, "timer interval should be a numeric value\n");
                    exit(1);
                }
                if (timer_interval < 0 || timer_interval > 1000000) {
                    fprintf(stderr,
                            "Timer interval should be "
                            "between 0 and 1000000\n");
                    exit(2);
                }
                break;
            case 'T':  // type of service
                if (opt_to_int(optarg, &i) < 0) {
                    fprintf(stderr, "TOS should be a numeric value\n");
                    exit(1);
                }
                if (tos) {
                    fprintf(stderr, "Please use TOS or DSCP, not both\n");
                    exit(2);
                }
                if (i < 0 || i >= 256) {
                    fprintf(stderr, "TOS should be between 0 and 255\n");
                    exit(2);
                }
                tos = i;
                break;
            case 'D':  // diff serv code point
                if (opt_to_int(optarg, &i) < 0) {
                    fprintf(stderr, "DSCP should be a numeric value\n");
                    exit(1);
                }
                if (tos) {
                    fprintf(stderr, "Please use TOS or DSCP, not both\n");
                    exit(2);
                }
                if (i < 0 || i >= 64) {
                    fprintf(stderr,
                            "DSCP should be between 0 and "
                            "63 (EF = 46)\n");
                    exit(2);
                }
                tos = i << 2;
                break;
            case 'S':
                source = optarg;
                break;
            case 'v':  // version number
                fprintf(stderr, "%s version %s\n", execname, VERSION);
                exit(1);
            case 'w':  // wireless parameters
#ifdef HAVE_LINUX_WIRELESS_H
                wireless = 1;
                if (opt_to_int(optarg, &rate_interval) < 0) {
                    fprintf(stderr, "Rate interval should be a numerical value\n");
                    exit(1);
                }
                break;
#else
                fprintf(stderr, "w option unavalaible under this system\n");
                exit(2);
#endif
            case '4':  // force IPv4
                force_ipv4 = 1;
                break;
            case '6':  // force IPv6
                force_ipv6 = 1;
#ifndef IPv6
                fprintf(stderr,
                        "Invalid -6 option, this system does "
                        "not support IPv6\n");
                exit(2);
#endif
                break;
            case 'c':
                bcast = 1;
                break;
            case 'V':
                sender_disp = 1;
                break;
            case 'f':  // test file
                testfilename = optarg;
                break;

            default:
                usage();
        }
    }
    if (optind != argc - 1)
        usage();
    hostname = argv[optind];

    if (force_ipv4 && force_ipv6) {
        fprintf(stderr, "Cannot specify both IPv6 and IPv4 protocol\n");
        exit(2);
    }

    hints.ai_flags = 0;
#ifdef IPv6
    if (force_ipv4) {
        hints.ai_family = AF_INET;
    } else if (force_ipv6) {
        hints.ai_family = AF_INET6;
    } else {
        hints.ai_family = AF_UNSPEC;
    }
#else
    hints.ai_family = AF_INET;
#endif
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_addrlen = 0;
    hints.ai_addr = NULL;
    hints.ai_canonname = NULL;
    hints.ai_next = NULL;

    if ((i = getaddrinfo(hostname, portname, &hints, &rai))) {
        fprintf(stderr, "getaddrinfo (%s): %s\n", __func__, gai_strerror(i));
        if (i == EAI_SYSTEM) {
            perror("getaddrinfo");
        }
        exit(1);
    }
#ifdef IPv6
    //  look for IPv6
    while (rai->ai_family != AF_INET6 && rai->ai_next != NULL) {
        rai = rai->ai_next;
    }
    if (rai->ai_family == AF_INET6) {
        default_pkt_sz = DEFAULT_PKT_SZ_6;
        fprintf(stderr, "IPv6, packet size defaults to %d\n", DEFAULT_PKT_SZ_6);
    } else if (rai->ai_family == AF_INET) {
        default_pkt_sz = DEFAULT_PKT_SZ_4;
        fprintf(stderr, "IPv4, packet size defaults to %d\n", DEFAULT_PKT_SZ_4);
    } else {
        fprintf(stderr, "Unknown protocol family %d, packet size defaults to %d\n", rai->ai_family, DEFAULT_PKT_SZ_4);
        default_pkt_sz = DEFAULT_PKT_SZ_4;
    }
#else
    default_pkt_sz = DEFAULT_PKT_SZ_4;
    fprintf(stderr, "IPv4, packet size defaults to %d\n", DEFAULT_PKT_SZ_4);
#endif

    //  verify args are coherent

    //  block and packet sizes
    if (b_burstsz) {
        if (p_burstsz) {
            if (pkt_sz && (b_burstsz != p_burstsz * pkt_sz)) {
                fprintf(stderr,
                        "Inconsistent specifications "
                        "of burst and packet zize\n");
                exit(2);
            }
            pkt_sz = b_burstsz / p_burstsz;
        }
        if (pkt_sz == 0)
            pkt_sz = default_pkt_sz;
        p_burstsz = b_burstsz / pkt_sz;  //  just in case
    } else {
        if (pkt_sz == 0)
            pkt_sz = default_pkt_sz;
        if (p_burstsz)
            b_burstsz = p_burstsz * pkt_sz;
        else {
            p_burstsz = DEFAULT_PKT_BURST_SZ;
            b_burstsz = pkt_sz;
        }
    }

    if (testfilename) {
        if ((test_file = fopen(testfilename, "r")) == NULL) {
            perror(testfilename);
            exit(1);
        }
        atstdsc = calloc(MAXNUMDESC, sizeof(struct tstdsc));
    }

    fprintf(stderr, "Packet size = %d bytes, burst size = %d bytes\n", pkt_sz, b_burstsz);

    // throughput and transmission interval
    if (rate) {
        krate = rate * K;
        ll = (long long)b_burstsz * 8 * 1000000;
        if (ius && ll / ius != krate) {
            fprintf(stderr,
                    "Inconsistent specification of Rate, "
                    "interval and burst size\n");
            exit(2);
        } else
            ius = ll / krate;
    } else if (ius) {
        rate = b_burstsz * 8;
        rate = rate * 1000000 / (ius * K);
    }

    if ((sock = socket(rai->ai_family, rai->ai_socktype, rai->ai_protocol)) < 0) {
        perror("Socket");
        exit(1);
    }

    if (source) {
        if (bind_source(sock, source, rai) < 0) {
            exit(1);
        }
    }

    if (set_tos(sock, tos) < 0) {
        exit(1);
    }

#ifdef HAVE_LINUX_WIRELESS_H
    if (wireless) {
        if (select_wireless_interface(s) <= 0)
            fprintf(stderr, "No wireless interface\n");
        else
            fprintf(stderr, "Wireless interface : %s\n", interface_name);
        bitrate = 0;
    }
#endif

    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &(bcast), sizeof(int)) < 0) {
        perror("Setsockopt Broadcast");
        exit(1);
    }

    pkt_hdr = (struct pkt_hdr *)buf;
    pkt_hdr->seqnum = 0;

    if (sender_disp) {
        printf(
            "\nTime         Packets    Total       |    Kbit/s    "
            "Avg 10    Avg\n");
    }

    // Prepare signals handling.
    struct sigaction sa;
    sa.sa_flags = 0;
    if (sigemptyset(&sa.sa_mask) < 0) {
        perror("sigemptyset");
        exit(1);
    }

    // First ignore SIGPIPE.
    sa.sa_handler = SIG_IGN;
    if (sigaction(SIGPIPE, &sa, NULL) < 0) {
        perror("Sigaction");
        exit(1);
    }

    // Prepare a pipe to know when we should leave the loop.
    int pipe_fds[2];
    if (pipe(pipe_fds) < 0) {
        perror("pipe");
        exit(1);
    }
    pipe_w = pipe_fds[1];

    // Hookup SIGINT and SIGTERM to quit();
    sa.sa_handler = quit_handler;
    if (sigaction(SIGINT, &sa, NULL) < 0) {
        perror("sigaction(SIGINT, quit_handler)");
        exit(1);
    }
    if (sigaction(SIGTERM, &sa, NULL) < 0) {
        perror("sigaction(SIGTERM, quit_handler)");
        exit(1);
    }
    if (sigaction(SIGTERM, &sa, NULL) < 0) {
        perror("sigaction(SIGTERM, quit_handler)");
        exit(1);
    }

    // Sets of file descriptors used with select.
    fd_set read_fds, write_fds;

    gettimeofday(&start_t, NULL);
    display_t = start_t;
    display_t.tv_sec++;
    stop_t = start_t;
    stop_t.tv_sec += duration;

    cycle = 0;
    total10 = 0;
    for (i = 0; i < SMOOTH_NB; i++) {
        smooth_win[i].nb_pkt = 0;
        smooth_win[i].time = start_t;
    }

    nb_sent = 1;

    rate_control_t = start_t;

    // Continuous test loop.
    if (!ius && !test_file) {
        int end = 0;
        int err = 0;
        int nfds = ((sock > pipe_fds[0]) ? sock : pipe_fds[0]) + 1;

        end = send_one_burst();
        while (!quit && !end) {
            FD_ZERO(&read_fds);
            FD_SET(pipe_fds[0], &read_fds);
            FD_ZERO(&write_fds);
            FD_SET(sock, &write_fds);

            err = select(nfds, &read_fds, &write_fds, NULL, NULL);
            if (err < 0 && errno != EINTR) {
                perror("pselect");
                exit(1);
            }
            if (FD_ISSET(sock, &write_fds)) {
                end = send_one_burst();
            }
            if (FD_ISSET(pipe_fds[0], &read_fds)) {
                // Leave the loop.
                end = 1;
            }
        }
        // Ensure the receiver gets notified.
        send_magic_packet();
        nb_sent--;
        // Print the results and flush everything.
        terminate();
        return 0;
    }

    // Time dependant test.
    int delay = 0;
    if (ius) {
        delay = ius;
        fprintf(stderr,
                "Rate = %lld Kbit/s, inter transmission "
                "interval = %d microseconds\n",
                rate, ius);
    } else if (test_file) {
        read_test_file(test_file, atstdsc);
        delay = delay_from_test_file(atstdsc, TEST_UDP);
    }

    // Check the timer is correct.
    if (timer_calibrate(timer_interval) < 0) {
        // FIXME: what should we do if the timer is incorrect ?
        exit(1);
    }

    struct ticker *t;
    t = ticker_create(timer_interval, ticker_handler, 1);
    if (!t) {
        fprintf(stderr, "failed to create the ticker");
        exit(1);
    }

    gettimeofday(&current_t, NULL);
    add_time(&next_event, &current_t, delay);
    if (ticker_start(t) < 0) {
        fprintf(stderr, "failed to start the ticker");
        exit(1);
    }

    // Prepare an environement to use select and signals.
    sigset_t empty_sigset;
    sigemptyset(&empty_sigset);

    while (!quit) {
        FD_ZERO(&read_fds);
        FD_SET(pipe_fds[0], &read_fds);

        int err = pselect(pipe_fds[0] + 1, &read_fds, NULL, NULL, NULL, &empty_sigset);
        if (err < 0 && errno != EINTR) {
            perror("select");
            exit(1);
        }
        if (err < 0 && errno == EINTR) {
            // Interrupted by a signal, nothing to do.
            continue;
        }
        if (FD_ISSET(pipe_fds[0], &read_fds)) {
            // The pipe was closed, just leave.
            break;
        }
    }
    ticker_stop(t);
    ticker_free(t);
    // Ensure the receiver gets notified.
    send_magic_packet();
    nb_sent--;
    // Print the results and flush everything.
    terminate();
    return 0;  // never reached
}
