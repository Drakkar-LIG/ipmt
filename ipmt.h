/**************************************************************************
 *
 * $Id$
 *
 * Created by Gilles Berger Sabbatel
 *
 * global definitions for the ipmt package
 *
 * Copyright LIG Laboratory (2002) Gilles Berger Sabbatel
 *
 * This software is a computer program whose purpose is to provide a set
 * of tools to measure performance of networks at the transport level
 * (TCP/IP and UDP/IP)..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 **************************************************************************/

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "config.h"

#ifdef __FreeBSD__
#include <netinet/in_systm.h>
#endif
#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#ifndef HAVE_GETADDRINFO
#include "getaddrinfo.h"
#endif
#ifdef HAVE_LINUX_WIRELESS_H
#include <linux/types.h>
#include <linux/wireless.h>
#include <net/if.h>
#endif
#include <libgen.h>
#include <stdlib.h>

#define STR_HELPER(x) #x
#define STR(x)        STR_HELPER(x)

#define K                    1000
#define SIZEBUF              (64 * 1024)  //  buffer size
#define MAGIC_SIZE           16           //  size of magic packet
#define MAX_OCTETS           (64 * 1024)  //  maximum packet size
#define SMOOTH_NB            10           //  smooth window size
#define DEFAULT_PKT_SZ_TCP   1460
#define DEFAULT_PKT_SZ_4     1472
#define DEFAULT_PKT_SZ_6     1452
#define DEFAULT_PORT         "13000"
#define DEFAULT_PKT_BURST_SZ 1

//  Default interval between interrupts.
// This does not seem to be limited by the kernel HZ ??? gnah ?
#define MIN_INTERVAL 50
#define NBCAL        200

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define TEST_TCP 0
#define TEST_UDP 1

#define Kbits(nbpkt) (((8 * (double)(pkt_sz * nbpkt))) / K)

extern char *hostname;  // Host destination
extern char buf[SIZEBUF];
extern char interface_name[256];
extern char execname[256];
extern char *portname;  // port number -- has to be long enough to fit most services in /etc/services :-)
extern char *source;    // force source [host][:port]

extern unsigned nb_sent;  // nb of sent pkts

extern struct timeval start_t,  // time at the start of the test
    stop_t,                     // time at the end of the test
    end_t,                      // time of the expected end of the test
    current_t,                  // intermediary
    display_t,                  // stat display time
    next_event;                 // next transmission

extern struct timeval rate_control_t;  // last rate control

int posterieur(struct timeval t1, struct timeval t2);

extern char sender_disp;  // Decide if stats should be displayed on sender side

extern int duration,  // test duration
    sock,             // socket fd
    bitrate,          // wireless bitrate
    total10,          // data smoothing
    rate_interval, new_bitrate;
extern unsigned int pkt_sz,  // packet size in bytes
    b_burstsz;               // bursts size (bytes)

// used for smoothing
struct pkt_bin {
    int nb_pkt;
    struct timeval time;
};
extern struct pkt_bin smooth_win[];

extern int cycle;   // current cycle (smoothing)
extern int nb_pkt;  // Nb of packets to send ( -1 : infinite)

#ifdef AF_INET6
#define IPv6
#endif
#define FIRST_IN_BURST 1
#define LAST_IN_BURST  2
#define FIRST_IN_BATCH 4
#define LAST_IN_BATCH  8

extern int passive_sock;  // passive socket
extern int force_ipv4, force_ipv6;

struct pkt_hdr {
    uint32_t seqnum;
    uint32_t ts_pkt_sec;
    uint32_t ts_pkt_usec;
    char flags;
};
extern struct pkt_hdr *pkt_hdr;

extern struct sigaction sigact;

void terminate();

double kbits(unsigned int nbpkt);
void finish();
void display_stats();
void add_time(struct timeval *sum, struct timeval *t1, int delta);

double t_elapse(struct timeval t1, struct timeval t2);

int bind_source(int sock, char *source, struct addrinfo *pai);
int set_tos(int sock, int tos);
int opt_to_int(char *string, int *opt);
int opt_to_uint(char *string, unsigned int *opt);
int opt_to_long(char *string, long *opt);
int opt_to_longlong(char *string, long long *opt);

/* Everything related to timers  */
void timer_init(struct itimerval *itv, int timer_interval);
int timer_calibrate(int interval);
