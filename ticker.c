/***************************************************************************
 *
 *  $Id$
 *
 *  Created by Damien Dejean
 *
 *  This file is part of the ipmt package
 *
 * This software is a computer program whose purpose is to provide a set
 * of tools to measure performance of networks at the transport level
 * (TCP/IP and UDP/IP)..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 ***************************************************************************/

#include "ticker.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

struct ticker {
    sigset_t old_sigset;
    struct sigaction old_sa;
    struct itimerval old_timer;
    struct itimerval timer;
    void (*handler)(int);
    int masked;
};

struct ticker *ticker_create(int delay, void (*handler)(int), int masked) {
    struct ticker *t = calloc(1, sizeof(struct ticker));
    if (!t) {
        return NULL;
    }
    t->timer.it_interval.tv_sec = 0;
    t->timer.it_interval.tv_usec = delay;
    t->timer.it_value.tv_sec = 0;
    t->timer.it_value.tv_usec = delay;
    t->handler = handler;
    return t;
}

void ticker_free(struct ticker *t) {
    if (!t) {
        return;
    }
    free(t);
}

int ticker_start(struct ticker *t) {
    struct sigaction ticker_sa;
    sigset_t mask_set;

    ticker_sa.sa_flags = 0;
    sigemptyset(&ticker_sa.sa_mask);
    ticker_sa.sa_handler = t->handler;

    // Setup the alarm handler.
    if (sigaction(SIGALRM, &ticker_sa, &t->old_sa) < 0) {
        perror("sigaction");
        return -1;
    }

    if (t->masked) {
        sigemptyset(&t->old_sigset);
        sigemptyset(&mask_set);
        sigaddset(&mask_set, SIGALRM);
        if (sigprocmask(SIG_BLOCK, &mask_set, &t->old_sigset) < 0) {
            perror("sigprocmask");
            goto undo_sigaction;
        }
    }

    // Setup the timer.
    if (setitimer(ITIMER_REAL, &t->timer, &t->old_timer) < 0) {
        perror("setitimer");
        goto undo_mask;
    }

    return 0;

undo_mask:
    if (t->masked) {
        if (sigprocmask(SIG_SETMASK, &t->old_sigset, NULL) < 0) {
            perror("sigprocmask");
        }
    }
undo_sigaction:
    if (sigaction(SIGALRM, &t->old_sa, NULL)) {
        perror("sigaction");
    }
    return -1;
}

int ticker_wait(struct ticker *t) {
    if (!t->masked) {
        return 0;
    }
    return sigsuspend(&t->old_sigset);
}

int ticker_stop(struct ticker *t) {
    int status = 0;
    if (sigaction(SIGALRM, &t->old_sa, NULL) < 0) {
        perror("sigaction");
        status = -1;
    }
    if (sigprocmask(SIG_SETMASK, &t->old_sigset, NULL)) {
        perror("sigaction");
        status = -1;
    }
    if (setitimer(ITIMER_REAL, &t->old_timer, NULL) < 0) {
        perror("setitimer");
        status = 1;
    }
    return status;
}
