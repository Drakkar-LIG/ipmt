/***************************************************************************
 *
 *  $Id$
 *
 *  Created by Damien Dejean
 *
 *  This file is part of the ipmt package
 *
 * This software is a computer program whose purpose is to provide a set
 * of tools to measure performance of networks at the transport level
 * (TCP/IP and UDP/IP)..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 ***************************************************************************/

#ifndef _TICKER_H_
#define _TICKER_H_

struct ticker;

// ticker_create allocates a ticker configured to call |handler| every |delay|
// us. If |masked| is non-zero, the ticker doesn't tick until a call to
// ticker_wait() is done.
struct ticker *ticker_create(int delay, void (*handler)(int), int masked);

// ticker_free releases the ticker structure and its dependencies.
void ticker_free(struct ticker *t);

// ticker_start installs the ticker alarm and callback into the system. If not
// masked, the ticking will start right after the call.
int ticker_start(struct ticker *t);

// ticker_stop uninstalls the ticker from the system and restores the previous
// handler, mask and timer present before the call to ticker_start().
int ticker_stop(struct ticker *t);

// ticker_wait blocks until the a tick happen. The behavior is similar to
// sigsuspend. If the ticker is configured as not masked, the function returns
// immediately 0, otherwise it returns the value of sigsuspend() and errno is
// set (see man sigsuspend).
int ticker_wait(struct ticker *t);

#endif  // _TICKER_H_