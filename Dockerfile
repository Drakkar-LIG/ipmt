# docker build -t drakkar/ipmt/builder:v1 .
# enable ipv6 and run
# docker run -it --rm -v ~/dev/ipmt:/src --sysctl net.ipv6.conf.all.disable_ipv6=0 drakkar/ipmt/builder:v1 /bin/bash
FROM gcc
RUN \
    apt-get update \
 && apt-get install -y net-tools flex bison
WORKDIR /src
CMD ./configure && make check
