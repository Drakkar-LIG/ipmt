/***************************************************************************
 *
 * $Id$
 *
 * Common functions for tcpmt and udpmt
 *
 * Created by Gilles Berger Sabbatel
 *
 * This file is part of the ipmt package
 *
 * Copyright LIG Laboratory (2007)
 *
 * This software is a computer program whose purpose is to provide a set
 * of tools to measure performance of networks at the transport level
 * (TCP/IP and UDP/IP)..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ************************************************************************/

#include "ipmt.h"
#include "ticker.h"

char *hostname;  // destination host
char buf[SIZEBUF];
char interface_name[256];
char execname[256];
char *portname;  // port number
char *source;    // force source [host][:port]

unsigned nb_sent;  // nb of sent pkts

struct timeval start_t,  // time at the start of the test
    stop_t,              // time at the end of the test
    end_t,               // time of the expected end of the test
    current_t,           // intermediary
    display_t,           // stat display time
    next_event;          // next transmission

struct timeval rate_control_t;  // last rate control

char sender_disp;  // Decide if stats should be displayed on sender side

int duration,  // test duration
    sock,      // socket fd
    bitrate,   // wireless bitrate
    total10,   // data smoothing
    rate_interval, new_bitrate;
unsigned int pkt_sz,  // packet size in bytes
    b_burstsz;        // bursts size (bytes)

struct pkt_bin smooth_win[SMOOTH_NB];

int cycle;   // current cycle (smoothing)
int nb_pkt;  // Nb of packets to send ( -1 : infinite)

int passive_sock;  // passive socket
int force_ipv4, force_ipv6;

struct pkt_hdr *pkt_hdr;

struct sigaction sigact;

struct tstdsc *tstdsc;
struct tstdsc *atstdsc;

// kbits converts a number of packets into Kilo bits (kilo = 1000).
double kbits(unsigned int nbpkt) {
    double r;

    r = nbpkt;
    r = r * pkt_sz * 8 / K;
    return (r);
}

// tsprint displays a timestamp.
void tsprint() {
    struct timeval tv;
    struct timezone tz;
    int s, ms;

    gettimeofday(&tv, &tz);
    s = tv.tv_sec % (24 * 3600);
    ms = tv.tv_usec / 1000;
    printf("%d.%03d     ", s, ms);
}

#ifdef HAVE_LINUX_WIRELESS_H

// code for wireless statistics : used to work on former versions of
// Linux, but unmaintained for a long time...
// Kept as a template, just in case...

/*
 *  find a wireless interface, for wireless statistics
 */

void wireless_stats() {
    int i, tss1, tsms1, tss2, tsms2;  // unused variables according to -Wall
    struct iwreq wrq;
    int r1, r2;
    i = (current_t.tv_sec - rate_control_t.tv_sec) * 1000 + (current_t.tv_usec - rate_control_t.tv_usec) / 1000;
    if (i >= rate_interval) {
        rate_control_t = current_t;
        strcpy(wrq.ifr_name, interface_name);
        if (ioctl(s, SIOCGIWRATE, &wrq) >= 0) {
            tss2 = current_t.tv_sec % (24 * 3600);
            tsms2 = current_t.tv_usec / 1000;
            tss1 = tss2;
            tsms1 = tsms2 - 10;
            if (tsms1 < 0) {
                tss1--;
                tsms1 += 1000;
            }
            new_bitrate = wrq.u.bitrate.value;
            if (bitrate != new_bitrate) {
                r1 = bitrate / 1000;
                r2 = new_bitrate / 1000;
                bitrate = new_bitrate;
                printf("%d.%03d, Seq = %d, bitrate = %d\n", tss1, tsms1, nb_sent - 1, r1);
                printf("%d.%03d, Seq = %d, bitrate = %d\n", tss2, tsms2, nb_sent, r2);
            }
        } else
            fprintf(stderr, "SIOCGIWRATE failed\n");
    }
}

int select_wireless_interface(int wl_sock) {
    struct iwreq wrq;
    char buff[1024];
    struct ifconf ifc;
    struct ifreq *ifr;
    int i;

    ifc.ifc_len = sizeof(buff);
    ifc.ifc_buf = buff;
    if (ioctl(wl_sock, SIOCGIFCONF, &ifc) < 0) {
        fprintf(stderr, "SIOCGIFCONF: %s\n", strerror(errno));
        return -1;
    }

    /*
     * We select the first wireless interface from the list of network
     * interfaces.
     *
     * We should check that the interface is actually the interface
     * used for the transmission, but it is rather uncommon to have
     * multiple wireless interfaces, and it hard to implement.
     */

    ifr = ifc.ifc_req;
    for (i = ifc.ifc_len / sizeof(struct ifreq); --i >= 0; ifr++) {
        strcpy(wrq.ifr_name, ifr->ifr_name);
        if (ioctl(wl_sock, SIOCGIWNAME, &wrq) >= 0) {
            strcpy(interface_name, ifr->ifr_name);
            return 1;
        }
    }
    return -1;
}
#endif

// posterieur checks if t1 is later than t2.
int posterieur(struct timeval t1, struct timeval t2) {
    if (t1.tv_sec > t2.tv_sec)
        return (TRUE);
    if (t1.tv_sec == t2.tv_sec)
        return (t1.tv_usec > t2.tv_usec);
    return (FALSE);
}

/*
 * end of the test (normal or ^C)
 */

void terminate() {
    double kbps;

    close(sock);
    gettimeofday(&end_t, NULL);

    printf("------- %s statistics -------\n", execname);
    printf("%u blocks of %u bytes to (or from) %s in %.5lf seconds.\n", nb_sent, pkt_sz, hostname,
           t_elapse(start_t, end_t));
    if (nb_sent > 0) {
        kbps = kbits(nb_sent) / t_elapse(start_t, end_t);
        printf("Avg throughput = %.0f kbit/s\n", kbps);
    }

    fflush(stdout);
    exit(0);
}

/*
 *  display statistics
 */

void display_stats() {
    int next_c;  // cycle suivant (lissage)
    double kbps1, kbps10, kbps;
    if (sender_disp == 1) {
        if (nb_sent == nb_pkt || !posterieur(display_t, current_t)) {
            kbps1 = kbits(smooth_win[cycle].nb_pkt) / t_elapse(smooth_win[cycle].time, current_t);
            kbps = kbits(nb_sent) / t_elapse(start_t, current_t);

            total10 += smooth_win[cycle].nb_pkt;
            next_c = (cycle + 1) % SMOOTH_NB;
            kbps10 = kbits(total10) / t_elapse(smooth_win[next_c].time, current_t);

            tsprint();
            printf("%d	%d	   |	%.0f	%.0f	%.0f\n", smooth_win[cycle].nb_pkt, nb_sent, kbps1, kbps10, kbps);
            fflush(stdout);

            while (!posterieur(display_t, current_t)) display_t.tv_sec++;
            cycle = next_c;
            total10 -= smooth_win[cycle].nb_pkt;
            smooth_win[cycle].nb_pkt = 0;
            smooth_win[cycle].time = current_t;
        }
    } else {  // no display
        if (!posterieur(display_t, current_t)) {
            cycle = (cycle + 1) % 3;
            switch (cycle) {
                case 0:
                    printf("\b\b\b- -");
                    break;  // \b
                case 1:
                    printf("\b\b\b\\ \\");
                    break;
                case 2:
                    printf("\b\b\b/ /");
                    break;
            }
            fflush(stdout);
            while (!posterieur(display_t, current_t)) display_t.tv_sec++;
        }
    }
    if (duration && ((current_t.tv_sec > stop_t.tv_sec) ||
                     ((current_t.tv_sec == stop_t.tv_sec) && (current_t.tv_usec >= stop_t.tv_usec)))) {
        terminate();
    }
}

// opt_to_int converts a string option to an int. Return 0 on success, an
// negative value on failure.
int opt_to_int(char *string, int *opt) {
    long l;
    if (opt_to_long(string, &l) < 0) {
        return -1;
    }
    *opt = (int)l;
    return 0;
}

// opt_to_long converts a string option to a long. Return 0 on success, an
// negative value on failure.
int opt_to_long(char *string, long *opt) {
    long l;
    char *pc;
    if (!opt) {
        return -1;
    }
    l = strtol(string, &pc, 0);
    if (pc != optarg + strlen(optarg)) {
        return -1;
    }
    *opt = l;
    return 0;
}

// opt_to_uint converts a string option to an unsigned int. Return 0 on success,
// an negative value on failure.
int opt_to_uint(char *string, unsigned int *opt) {
    unsigned int ui;
    char *pc;
    if (!opt) {
        return -1;
    }
    ui = strtoul(string, &pc, 0);
    if (pc != optarg + strlen(optarg)) {
        return -1;
    }
    *opt = ui;
    return 0;
}

// opt_to_longlong converts a string option to a long long int. Return 0 on
// success, an negative value on failure.
int opt_to_longlong(char *string, long long *opt) {
    long long ll;
    char *pc;
    if (!opt) {
        return -1;
    }
    ll = strtoll(string, &pc, 0);
    if (pc != optarg + strlen(optarg)) {
        return -1;
    }
    *opt = ll;
    return 0;
}

// t_elapse computes elapsed time in seconds from two dates.
double t_elapse(struct timeval t1, struct timeval t2) {
    t2.tv_sec -= t1.tv_sec;
    t2.tv_usec -= t1.tv_usec;

    if (t2.tv_usec < 0) {
        t2.tv_sec--;
        t2.tv_usec += 1000000;
    }

    return ((double)t2.tv_sec + (double)t2.tv_usec / 1000000.);
}

int bind_source(int sock, char *source, struct addrinfo *pai) {
    int i;
    char *portname;
    struct addrinfo hints, *rai = NULL;
    struct sockaddr *sockin_addr;
    socklen_t namelen;

    // split [host][:port]
    if ((portname = strrchr(source, ':'))) {
        *portname++ = '\0';
        if (*portname == '\0') {
            portname = NULL;
        }
    }
    if (*source == '\0') {
        source = NULL;
    }
    if (source == NULL && portname == NULL) {
        return 0;
    }
    fprintf(stderr, "Binding to source: %s:%s\n", source, portname);

    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = pai->ai_family;
    hints.ai_socktype = pai->ai_socktype;
    hints.ai_protocol = pai->ai_protocol;
    hints.ai_addrlen = 0;
    hints.ai_addr = NULL;
    hints.ai_canonname = NULL;
    hints.ai_next = NULL;

    if ((i = getaddrinfo(source, portname, &hints, &rai))) {
        fprintf(stderr, "getaddrinfo (%s): %s\n", __func__, gai_strerror(i));
        if (i == EAI_SYSTEM) {
            perror("getaddrinfo");
        }
        return -1;
    }

    sockin_addr = rai->ai_addr;
    namelen = rai->ai_addrlen;

    if (bind(sock, sockin_addr, namelen) < 0) {
        perror("bind");
        return -1;
    }
    return 0;
}

// set_tos sets Type-Of-Service or class.
int set_tos(int sock, int tos) {
    struct sockaddr sa;
    int res = sizeof(sa);

    if (tos != 0) {
        if (getsockname(sock, &sa, (socklen_t *)&res) < 0) {
            perror("getsockname");
            return -1;
        }
        switch (sa.sa_family) {
            case AF_INET:
                res = setsockopt(sock, IPPROTO_IP, IP_TOS, &tos, (socklen_t)sizeof(tos));
                break;
            case AF_INET6:
                res = setsockopt(sock, IPPROTO_IPV6, IPV6_TCLASS, &tos, (socklen_t)sizeof(tos));
                break;
            default:
                fprintf(stderr, "Warning: TOS/Class ignored for this protocol family (%d)\n", sa.sa_family);
                res = 0;
        }
        if (res != 0) {
            perror("setsockopt TOS");
            return -1;
        }
    }
    return 0;
}

// timer_init prepares the timer |itv| with the interval provided by
// |timer_interval|.
void timer_init(struct itimerval *itv, int timer_interval) {
    itv->it_interval.tv_sec = 0;
    itv->it_interval.tv_usec = timer_interval;
    itv->it_value.tv_sec = 0;
    itv->it_value.tv_usec = timer_interval;
}

volatile int samples[NBCAL];
volatile int ntics = 0;
volatile struct timeval before;

static void timer_alarm() {
    struct timeval now;
    if (ntics >= NBCAL) {
        // We exceeded the count of desired samples, let's ignore the event
        // while timer_calibrate stops the timer.
        return;
    }
    gettimeofday(&now, NULL);
    samples[ntics] = (now.tv_sec - before.tv_sec) * 1000000 + now.tv_usec - before.tv_usec;
    ntics++;
    before = now;
}

static int intcomp(const void *pi, const void *pj) { return (*(const int *)pi - *(const int *)pj); }

// timer_calibrate verifiers the precision of the timer.
int timer_calibrate(int interval) {
    int time = 0;
    int i;
    struct ticker *t;

    fprintf(stderr, "Calibrating timer...\n");

    t = ticker_create(interval, timer_alarm, 1);
    if (ticker_start(t) < 0) {
        return -1;
    }
    gettimeofday((struct timeval *)&before, NULL);
    while (ntics < NBCAL) {
        // Unmask and wait for an alarm.
        ticker_wait(t);
        // Back to critical section here, alarm masked.
    }
    if (ticker_stop(t) < 0) {
        fprintf(stderr, "failed to stop the ticker");
        return -1;
    }
    ticker_free(t);
    t = NULL;

    // Sort the interval table.
    qsort((void *)samples, (size_t)NBCAL, (size_t)sizeof(int), intcomp);
    // Mean interval that excludes the 20 smallest and greatest values.
    for (i = 20; i < NBCAL - 20; i++) {
        time += samples[i];
    }
    time = time / (NBCAL - 40);

    // FIXME: what should we do with the computed time ?

    fprintf(stderr, "Actual timer duration = %d microseconds\n", time);
    return 0;
}

// add_time adds an interval in microseconds to a timeval
void add_time(struct timeval *sum, struct timeval *t1, int delta) {
    int dsec, dusec;

    dsec = delta / 1000000;
    dusec = delta % 1000000;
    sum->tv_sec = t1->tv_sec + dsec;
    sum->tv_usec = t1->tv_usec + dusec;
    if (sum->tv_usec >= 1000000) {
        sum->tv_usec -= 1000000;
        sum->tv_sec++;
    } else if (sum->tv_usec < 0) {
        sum->tv_usec += 1000000;
        sum->tv_sec--;
    }
}
