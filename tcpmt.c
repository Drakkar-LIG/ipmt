/****************************************************************************
 *
 * $Id$
 *
 * created by Gilles Berger Sabbatel
 *
 * This file is part of the ipmt package
 *
 * Copyright LIG Laboratory (2007)
 *
 * This software is a computer program whose purpose is to provide a set
 * of tools to measure performance of networks at the transport level
 * (TCP/IP and UDP/IP)..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 ***************************************************************************/

#include <netinet/tcp.h>  // for TCP_NODELAY
#include <signal.h>
#include <sys/select.h>

#include "ipmt.h"
#include "tcpcommon.h"
#include "testfile.h"
#include "ticker.h"

// variables for rate limited test option
static FILE *test_filehandle = 0;
static struct itimerval itv_tcp;
static long long ll_tcp, krate;
static int ius_tcp = 0;
static long long rate_tcp = 0;
static int timer_interval_tcp;

// Buffer for data reception (-t option)
static char sink_buf[64 * 1024];

char host[NI_MAXHOST], serv[NI_MAXSERV];

static int bufsz = 0;

static int child_pid;

static int tos = 0;  // type of service, declared here because of send_one_file_tf callback

static char query_mode = 0;

// Activate TCP no-delay option
static void enable_tcp_no_delay() {
    int flag = 1;
    int result = setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(int));
    if (result < 0) {
        perror("Couldn't setsockopt(TCP_NODELAY)\n");
        exit(EXIT_FAILURE);
    }
}

// Function to activate TCP NOPUSH
static void enable_tcp_no_push() {
    int flag = 1;
    int result = +1;
#ifdef TCP_CORK
    result = setsockopt(sock, IPPROTO_TCP, TCP_CORK, (char *)&flag, sizeof(int));
#elif defined TCP_NOPUSH
    result = setsockopt(sock, IPPROTO_TCP, TCP_NOPUSH, (char *)&flag, sizeof(int));
#endif
    if (result < 0) {
        perror("Couldn't setsockopt(TCP_NOPUSH | TCP_CORK)\n");
        exit(EXIT_FAILURE);
    } else if (result > 0) {
        perror(
            "Please note : I could not set either NOPUSH (BSD) or TCP_CORK (LINUX)\n\
        You may see small packets if the write size does not match the MSS\n");
    }
}

static void do_stats() {
    smooth_win[cycle].nb_pkt++;
    nb_sent++;
    gettimeofday(&current_t, NULL);
    display_stats();
}

static void send_data() {
    if (write(sock, buf, pkt_sz) < 0) {
        perror("write");
        terminate();
    }
    do_stats();
}

static long int count_received_data() {
    ssize_t bytes_received;
    long int total = 0;
    while (1) {
        while ((bytes_received = recv(sock, &buf, sizeof(buf), 0)) < 0) {
            if (errno == EAGAIN || errno == EINTR)
                continue;
            perror("Recv");
            exit(1);
        }
        if (bytes_received == 0) {
            return total;
        } else {
            total += bytes_received;
        }
    }
}

static void send_one_file_tf() {
    int frk;

    frk = fork();
    if (frk > 0) {  // I'm the parent process -- do nothing
        nb_sent += b_burstsz;
    } else if (frk == 0) {  // Child: send the data
        nb_sent = 0;

        sock = ipmt_connect(hostname, portname, source, bufsz);
        set_tos(sock, tos);
        gettimeofday(&start_t, NULL);
        if (query_mode) {
            char qry_str[256];
            long int bytes_received;
            sprintf(qry_str, "%u\t%u", b_burstsz, pkt_sz);
            if (write(sock, qry_str, strlen(qry_str)) < 0) {
                perror("write");
                terminate();
            }
            bytes_received = count_received_data();
            if (bytes_received != b_burstsz * pkt_sz) {
                printf("Incomplete transfer\n");
            } else {
                printf("Everything received\n");
                nb_sent = b_burstsz;
            }
        } else {
            while (nb_sent < b_burstsz) {
                if (write(sock, buf, pkt_sz) < 0) {
                    perror("write");
                    terminate();
                }
                nb_sent++;
            }
        }
        terminate();
        //         close(s);
        //         fflush(stdout);
        //         _exit(0);
    } else {
        perror("Fork");
        exit(1);
    }
}

void (*send_one_file_ptr)() = NULL;

void finish() {
    fflush(stdout);
    wait(NULL);
    exit(0);
}

/*
 * Compute time to the next burst, either for regular transmission, or
 * if reading a test file
 */
static int time_to_next_burst_tcp() {
    if (!test_filehandle) {
        return ius_tcp;
    } else {
        return 1000 * prepare_next_burst(0);
    }
}

// Function called when the alarm rings:
// Used for -r --> calls send_data()
//          -f --> calls send_one_file_tf()
static void alarm_catcher() {
    int delta = 0;
    struct timeval time;

    gettimeofday(&time, NULL);
    //  envoi de tous les bursts dont l'échéance est atteinte
    while (posterieur(time, next_event)) {
        (*send_one_file_ptr)();
        delta = time_to_next_burst_tcp();
        if (delta < 0) {
            // FIXME: do not use finish.
            finish();
        }
        add_time(&next_event, &next_event, delta);
    }
}

/*
 * Display usage message
 */

static void usage() {
    printf("Usage: %s [options] host\n"
               "Options :\n"
               "    -4       : force IPv4 protocol\n"
               "    -6       : force IPv6 protocol\n"
               "    -b bufsz : TCP send buffer size\n"
               "    -c       : Chop transmissions: PUSH the packets out at each write to the socket (default behavior of most applications) \n"
               "    -D dscp  : diff serv code point (RFC 2474)\n"
               "    -d secs  : test duration in seconds (default transmit until stopped)\n"
               "    -f file  : read a test file\n"
               "    -n datasz: number of bytes to send (default transmit until stopped)\n"
               "    -N pknb  : total number of packets to send (default: transmit until stopped)\n"
               "    -P       : passive - Connection establishment will come from tcptarget -A; host not used\n"
               "    -p port  : destination port number (default: " DEFAULT_PORT ")\n"
               "    -q       : used with -f, send the query to the other side (-R)\n"
               "    -R       : get amount of data to send from the cx itself (num_writes tab num_bytes) (the socket is passive)\n"
               "    -r rate  : rate in Kbit/s (K = " STR(K) ")\n"
               "    -S source: [host][:port] format\n"
               "    -s pktsz : write chunk size in bytes (default: " STR(DEFAULT_PKT_SZ_TCP) " ; 1 std MSS; although generally ts option gets in the way)\n"
               "    -T tos   : type of service (deprecated: RFC 1349, obsoleted by RFC 2474)\n"
               "    -t       : target - Start a traffic sink for bidirectionnal testing (use tcpmt on both sides)\n"
               "    -v       : display version number and exit\n"
               "    -y       : tcp_nodelaY: de-activate Nagle algorithm \n",
               execname);
    exit(1);
}

static void read_data(int fildes) {
    ssize_t bytes_received;

    while ((bytes_received = recv(fildes, &sink_buf, sizeof(sink_buf), 0)) < 0) {
        if (errno == EAGAIN || errno == EINTR)
            continue;
        perror("Recv");
        exit(1);
    }
    if (bytes_received == 0) {
        exit(0);
    }
}

static void stats_disp_header() {
    int i;
    printf("\nTime         Packets    Total	   |	Kbit/s	Avg 10	Avg\n");

    display_t = start_t;
    display_t.tv_sec++;
    stop_t = start_t;
    stop_t.tv_sec += duration;

    cycle = 0;
    total10 = 0;
    for (i = 0; i < SMOOTH_NB; i++) {
        smooth_win[i].nb_pkt = 0;
        smooth_win[i].time = start_t;
    }
}

static void finish_t() {
    if (child_pid) {
        if (kill(child_pid, SIGTERM) < 0) {
            perror("kill child process failed?\n");
            exit(1);
        }
    } else
        perror("What are we doing here if there is no child process?\n");
    terminate();
}

static ssize_t write_and_rec(int fd, const void *buf, size_t nbyte) {
    fd_set rset, wset;

    while (1) {
        FD_ZERO(&rset);
        FD_SET(fd, &rset);
        FD_ZERO(&wset);
        FD_SET(fd, &wset);

        select(fd + 1, &rset, &wset, 0, 0);
        if (FD_ISSET(fd, &rset)) {  // first see if there's anything to receive
            read_data(fd);
        }
        if (FD_ISSET(fd, &wset)) {
            return write(fd, buf, nbyte);
        }
    }
}

/*
 * MAIN
 */

int main(int argc, char *argv[]) {
    timer_interval_tcp = MIN_INTERVAL;
    ll_tcp = 0;
    krate = 0;
    ius_tcp = 0;
    rate_tcp = 0;

    int ch = 0, i;
    int flags = 0;
    long int data_sz = -1;
    socklen_t rlen;
    char chop = 0, nonagle = 0, passive = 0, reactive = 0;
    struct sockaddr_storage sockr;

    pkt_sz = DEFAULT_PKT_SZ_TCP;
    nb_pkt = -1;
    force_ipv4 = 0;
    force_ipv6 = 0;
    int start_target = 0;
    sender_disp = 1;
    rate_tcp = 0;
    char *testfilename;
    testfilename = NULL;
    char defaultportname[] = DEFAULT_PORT;
    portname = defaultportname;
    struct ticker *t;

    send_one_file_ptr = &send_data;  // by default, limited rate

    strncpy(execname, basename(argv[0]), sizeof(execname) - 1);
    execname[sizeof(execname) - 1] = '\0';
    while ((ch = getopt(argc, argv, "d:p:r:s:S:n:N:PRT:D:cyv46b:tf:qh")) != -1) {
        switch (ch) {
            case 'd':  //  test duration
                if (opt_to_int(optarg, &duration) < 0) {
                    fprintf(stderr, "Duration should be a numeric value\n");
                    exit(1);
                }
                if (duration < 0) {
                    fprintf(stderr, "Duration should be >= 0\n");
                    usage();
                }
                break;
            case 'p':  // port number
                // strncpy(portname, optarg, sizeof(portname));
                portname = optarg;
                break;
            case 'r':  // bitrate
                if (opt_to_longlong(optarg, &rate_tcp) < 0) {
                    fprintf(stderr, "Rate should be a numeric value\n");
                    exit(1);
                }
                if (rate_tcp < 0) {
                    fprintf(stderr, "Rate should be > 0\n");
                    exit(2);
                }
                break;
            case 'S':
                source = optarg;
                break;
            case 's':  // write buffer size
                if (opt_to_uint(optarg, &pkt_sz) < 0) {
                    fprintf(stderr, "Packet size should be a numeric value\n");
                    exit(1);
                }
                if (pkt_sz <= 0 || pkt_sz > MAX_OCTETS) {
                    printf("%d: limit is %d\n", pkt_sz, MAX_OCTETS);
                    usage();
                }
                break;
            case 'N':  // number of blocks
                if (opt_to_int(optarg, &nb_pkt) < 0) {
                    fprintf(stderr, "Packet count should be a numeric value\n");
                    exit(1);
                }
                if (nb_pkt <= 0) {
                    printf("invalid number of packets\n");
                    usage();
                }
                break;
            case 'n':  // number of bytes
                if (opt_to_long(optarg, &data_sz) < 0) {
                    fprintf(stderr, "Data size should be a numeric value\n");
                    exit(1);
                }
                if (data_sz <= 0) {
                    printf("Invalid size\n");
                    usage();
                }
                break;
            case 'T':  // type of service
                if (opt_to_int(optarg, &i) < 0) {
                    fprintf(stderr, "TOS should be a numeric value\n");
                    exit(1);
                }
                if (tos) {
                    fprintf(stderr, "Please use TOS or DSCP, not both\n");
                    exit(2);
                }
                if (i < 0 || i >= 256) {
                    fprintf(stderr, "TOS should be between 0 and 255\n");
                    exit(2);
                }
                tos = i;
                break;
            case 'D':  // diff serv code point
                if (opt_to_int(optarg, &i) < 0) {
                    fprintf(stderr, "DSCP should be a numeric value\n");
                    exit(1);
                }
                if (tos) {
                    fprintf(stderr, "Please use TOS or DSCP, not both\n");
                    exit(2);
                }
                if (i < 0 || i >= 64) {
                    fprintf(stderr, "DSCP should be between 0 and 63 (EF = 46)\n");
                    exit(2);
                }
                tos = i << 2;
                break;
            case 'v':  // version number
                fprintf(stderr, "%s version %s\n", execname, VERSION);
                exit(0);
            case 'b':  // Buffer size
                if (opt_to_int(optarg, &bufsz) < 0) {
                    fprintf(stderr, "Buffer size should be a numeric value\n");
                    exit(1);
                }
                break;
            case 'P':  // TCP connection initiated by tcptarget
                passive = 1;
                break;
            case 'R':  // TCP connection initiated by tcptarget
                reactive = 1;
                break;
            case 'c':  // TCP NOPUSH
                chop = 1;
                break;
            case 'y':  // TCP NOPUSH
                nonagle = 1;
                break;
            case '4':  // force IPv4
                force_ipv4 = 1;
                break;
            case '6':  // force IPv6
                force_ipv6 = 1;
            case 't':  // start target
                start_target = 1;
#ifndef IPv6
                fprintf(stderr, "Invalid -6 option, this system does not support IPv6\n");
                exit(2);
#endif
                break;
            case 'f':  // test file
                testfilename = optarg;
                send_one_file_ptr = &send_one_file_tf;
                break;
            case 'q':  // test file
                query_mode = 1;
                break;

            default:
                usage();
        }
    }
    if (!(passive || reactive) && optind != argc - 1)
        usage();
    hostname = argv[optind];

    if (query_mode && !testfilename) {
        perror("-q option only valid with -f");
        exit(2);
    }

    // Check that incompatible options are not used together
    if (nb_pkt > 0 && data_sz > 0) {
        printf("You cannot specify the number of packets to send and the number of bytes to send at the same time !\n");
        usage();
    } else if (data_sz > 0) {
        nb_pkt = data_sz / pkt_sz;
        data_sz = data_sz % pkt_sz;
        printf("will send %d full packets and then %ld bytes", nb_pkt, data_sz);
    }

    if (force_ipv4 && force_ipv6) {
        fprintf(stderr, "Cannot specify both IPv6 and IPv4 protocol\n");
        exit(2);
    }
    // throughput and transmission interval
    if (rate_tcp) {
        b_burstsz = pkt_sz;
        krate = rate_tcp * K;
        ll_tcp = (long long)b_burstsz * 8 * 1000000;
        if (ius_tcp && ll_tcp / ius_tcp != krate) {
            fprintf(stderr, "Inconsistent specification of Rate, interval and burst size\n");
            exit(2);
        } else
            ius_tcp = ll_tcp / krate;
    } else if (ius_tcp || test_filehandle) {
        rate_tcp = b_burstsz * 8;
        if (ius_tcp)
            rate_tcp = rate_tcp * 1000000 / (ius_tcp * K);
    }
    if (ius_tcp) {
        if (ius_tcp) {
            fprintf(stderr, "Rate = %lld Kbit/s, inter transmission interval = %d microseconds\n", rate_tcp, ius_tcp);
        }
        timer_init(&itv_tcp, timer_interval_tcp);
    }

    // Prepare signal handlers
    struct sigaction sa;
    if (sigemptyset(&sa.sa_mask) < 0) {
        perror("sigemptyset");
        exit(1);
    }
    sa.sa_flags = 0;
    sa.sa_handler = SIG_IGN;

    if (sigaction(SIGPIPE, &sa, NULL) < 0) {
        perror("sigaction(SIGPIPE)");
        exit(1);
    }

    sigact.sa_handler = terminate;
    if (sigaction(SIGINT, &sigact, NULL) < 0) {
        perror("sigaction(SIGINT, terminate)");
        exit(1);
    }

    gettimeofday(&start_t, NULL);

    if (testfilename || reactive) {
        // We do not wait for the exit status of the child process (that would create zombies!)
        sa.sa_flags = SA_NOCLDWAIT;
        sa.sa_handler = SIG_IGN;
        if (sigaction(SIGCHLD, &sa, NULL) < 0) {
            perror("sigaction(SIGCHLD, SIG_IGN)");
            exit(1);
        }
    }

    // reads the test file and starts from there...
    if (testfilename) {
        int delay;
        struct itimerval itv;

        test_filehandle = fopen(testfilename, "r");
        if (!test_filehandle) {
            perror(testfilename);
            exit(1);
        }
        atstdsc = calloc(MAXNUMDESC, sizeof(struct tstdsc));
        if (!atstdsc) {
            perror("calloc");
            exit(1);
        }
        read_test_file(test_filehandle, atstdsc);
        delay = delay_from_test_file(atstdsc, 0);
        fflush(stdout);

        timer_init(&itv, MIN_INTERVAL);
        timer_calibrate(MIN_INTERVAL);

        t = ticker_create(MIN_INTERVAL, alarm_catcher, 1);
        if (!t) {
            fprintf(stderr, "failed to create ticker\n");
            exit(1);
        }
        if (ticker_start(t) < 0) {
            fprintf(stderr, "failed to start ticker\n");
            exit(1);
        }
        fflush(stdout);
        gettimeofday(&current_t, NULL);
        add_time(&next_event, &current_t, delay);
        alarm_catcher();
        while (1) {
            ticker_wait(t);
        }
        ticker_stop(t);
        ticker_free(t);
        printf("Never reached\n");
        exit(1);
    }

    // Connect to hostname/portname, if not passive tcpmt
    if (!passive && !reactive)
        sock = ipmt_connect(hostname, portname, source, bufsz);
    else {
        passive_sock = ipmt_listen(portname, bufsz);
        rlen = sizeof(sockr);
        if ((sock = accept(passive_sock, (struct sockaddr *)&sockr, &rlen)) < 0) {
            perror("accept");
            exit(2);
        }
        flags = NI_NUMERICHOST | NI_NUMERICSERV;
        if ((i = getnameinfo((struct sockaddr *)&sockr, rlen, host, sizeof(host), portname, sizeof(portname), flags)) <
            0) {
            fprintf(stderr, "getnameinfo: %s\n", gai_strerror(i));
            exit(2);
        }
        hostname = host;
        printf("Accepted host %s\n", host);
        if (reactive) {
            while (1) {
                int frk;
                frk = fork();
                if (frk > 0) {  // I'm the parent process
                    close(sock);
                    sock = accept(passive_sock, (struct sockaddr *)&sockr, &rlen);
                    if (sock < 0) {
                        perror("Accept");
                        exit(1);
                    }
                } else if (frk == 0) {
                    // I'm the child
                    break;
                } else {
                    perror("Fork");
                    exit(1);
                }
            }
            read_data(sock);
            // It should not be necessary to receive multiple times for such a small amount of data
            // Read num blocks / block size
            sscanf((char *)sink_buf, "%u\t%u", &nb_pkt, &pkt_sz);
            printf("send %u %u\n", nb_pkt, pkt_sz);
        }
    }

    if (set_tos(sock, tos) < 0) {
        exit(1);
    }

    if (start_target && ius_tcp) {  // forks a receiver if paced sending option

        printf("starting target\n");
        child_pid = fork();
        if (child_pid == 0) {  // The child receives the data
            while (1) {
                read_data(sock);
            }
            printf("Never reached\n");
            exit(1);
        }
        sigact.sa_handler = finish_t;
        if (sigaction(SIGTERM, &sigact, NULL) < 0) {  // so a simple TERM terminates the child...
            perror("Sigaction - SIGTERM");
            exit(1);
        }
    }
    if (!reactive)
        stats_disp_header();
    nb_sent = 0;

    // Adjusts sock opts depending on platform
    if (!chop)
        enable_tcp_no_push();
    if (!nonagle)
        enable_tcp_no_delay();

    /*
     * main loop of the test
     */

    if (ius_tcp) {  // test temporisé
        gettimeofday(&current_t, NULL);
        add_time(&next_event, &current_t, ius_tcp);
        t = ticker_create(ius_tcp, alarm_catcher, 1);
        if (!t) {
            fprintf(stderr, "failed to create the ticker\n");
            exit(1);
        }
        if (ticker_start(t) < 0) {
            fprintf(stderr, "failed to start the ticker\n");
            exit(1);
        }
        alarm_catcher();
        while (1) {
            ticker_wait(t);
        }
        ticker_stop(t);
        ticker_free(t);
    } else {
        // function pointer to either send only, or receive as well
        ssize_t (*_write_and_rec)(int, const void *, size_t);

        // Adjust sending (+receiving) function
        if (!start_target)
            _write_and_rec = write;  // We are juste sending data
        else {
            _write_and_rec = write_and_rec;  // we want to receive, too!
        }
        while (nb_pkt < 0 || nb_sent < nb_pkt) {
            if ((_write_and_rec)(sock, buf, pkt_sz) < 0) {
                perror("write");
                terminate();
                break;
            }

            if (!reactive) {
                do_stats();
            } else
                nb_sent++;  // done in do_stats() otherwise
        }

        // If a size in bytes was entered, send the remainder
        if (data_sz > 0) {
            if ((*_write_and_rec)(sock, buf, data_sz) < 0) {
                perror("write");
                terminate();
            }
            do_stats();
        }
    }
    if (!reactive) {
        terminate();
    }

    exit(0);  //  not reached - just to avoid warning
}
